﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(OrdaBurda.Startup))]
namespace OrdaBurda
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
